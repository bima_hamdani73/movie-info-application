import 'package:flutter/material.dart';
import 'package:movie_info_app/Screens/profile.dart';
import 'package:movie_info_app/Screens/top/top_screen.dart';

import 'Screens/home/home_screen.dart';

class Homenav extends StatefulWidget {
  Homenav({Key key, this.title}) : super(key: key);

  final String title;

  @override
  HomenavState createState() => HomenavState();
}

class HomenavState extends State<Homenav> {
  int currentTab = 0;
  final tabs = [HomeScreen(), TopScreen(), Profile()];
  @override
  // Widget currentScreen = Profile();
  Widget build(BuildContext context) {
    return Scaffold(
      body: tabs[currentTab],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: currentTab,
        type: BottomNavigationBarType.fixed,
        iconSize: 30,
        selectedFontSize: 17,
        unselectedFontSize: 13,
        backgroundColor: Colors.red,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text('Home'),
              backgroundColor: Colors.blue),
          BottomNavigationBarItem(
              icon: Icon(Icons.star),
              title: Text('Top'),
              backgroundColor: Colors.red),
          BottomNavigationBarItem(
              icon: Icon(Icons.person),
              title: Text('Profile'),
              backgroundColor: Colors.green)
        ],
        onTap: (index) {
          setState(() {
            currentTab = index;
          });
        },
      ),
    );
  }
}
