class TopMovie {
  final int id, year;
  final double rating;
  final List<String> genra;
  final String plot, title, poster, backdrop, pg, duration, genre;
  final List<Map> cast;

  TopMovie(
      {this.poster,
      this.backdrop,
      this.title,
      this.id,
      this.year,
      this.rating,
      this.genra,
      this.plot,
      this.cast,
      this.pg,
      this.duration,
      this.genre});
}

// our demo data movie data
List<TopMovie> moviesTop = [
  TopMovie(
    id: 1,
    title: "Hamilton",
    year: 2020,
    poster: "assets/images/poster1.jpg",
    backdrop: "assets/images/backdrop1.jpg",
    rating: 8.4,
    genre: "Drama",
    genra: ["Musical", "Drama"],
    pg: "PG-13",
    duration: "2h 40m",
    plot:
        "Alexander Hamilton, an orphan, arrives in New York to work for George Washington. After the American Revolution, he goes on to become first Secretary of the Treasury of the US.",
    cast: [
      {
        "orginalName": "William Eubank",
        "movieName": "Director",
        "image": "assets/images/actor_a5.jpg",
      },
      {
        "orginalName": "Kristen Stewart",
        "movieName": "Norah Price",
        "image": "assets/images/actor_a1.jpg",
      },
      {
        "orginalName": "Jessica Henwick",
        "movieName": "Emily",
        "image": "assets/images/actor_a2.jpg",
      },
      {
        "orginalName": "T.J. Miller",
        "movieName": "Paul",
        "image": "assets/images/actor_a3.jpg",
      },
      {
        "orginalName": "Vincent Cassel",
        "movieName": "W. Lucien",
        "image": "assets/images/actor_a4.jpg",
      },
    ],
  ),
  TopMovie(
    id: 2,
    title: "Onward",
    year: 2020,
    poster: "assets/images/poster2.jpg",
    backdrop: "assets/images/backdrop2.jpeg",
    rating: 7.4,
    genre: "Family",
    genra: ["Family", "Adventure"],
    pg: "PG-17",
    duration: "1h 42m",
    plot:
        "In a magical realm where technological advances have taken over, Ian and Barley, two elven brothers, set out on an epic adventure to resurrect their late father for a day.",
    cast: [
      {
        "orginalName": "Sam Hargrave",
        "movieName": "Director",
        "image": "assets/images/actor_b4.png",
      },
      {
        "orginalName": "Chris Hemsworth",
        "movieName": "Tyler Rake",
        "image": "assets/images/actor_b1.png",
      },
      {
        "orginalName": "Golshifteh Farahani",
        "movieName": "Nik Khan",
        "image": "assets/images/actor_b2.png",
      },
      {
        "orginalName": "Randeep Hooda",
        "movieName": "Saju Rav",
        "image": "assets/images/actor_b3.png",
      },
      {
        "orginalName": "Neha Mahajan",
        "movieName": "Neysa Rav",
        "image": "assets/images/actor_b5.png",
      },
      {
        "orginalName": "David Harbour",
        "movieName": "Gaspar",
        "image": "assets/images/actor_b6.png",
      },
    ],
  ),
  TopMovie(
    id: 3,
    title: "The King of Staten Island",
    year: 2020,
    poster: "assets/images/poster3.jpg",
    backdrop: "assets/images/backdrop3.jpeg",
    rating: 7.1,
    genre: "Comedy",
    genra: ["Comedy", "Drama"],
    duration: "2h 18m",
    pg: "PG-17",
    plot:
        "A 24-year-old Scott squanders his time while still coping with his firefighter father's death years back. He introspects his choices after his mother starts seeing Ray, who is also a fireman.",
    cast: [
      {
        "orginalName": "James Mangold",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Matt Damon",
        "movieName": "Carroll",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Christian Bale",
        "movieName": "Ken Miles",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Caitriona Balfe",
        "movieName": "Mollie",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 4,
    title: "The Invisible Man",
    year: 2020,
    poster: "assets/images/poster4.jpg",
    backdrop: "assets/images/backdrop4.jpg",
    genre: "Horror",
    rating: 7.1,
    genra: ["Horror", "Thriller"],
    duration: "2h 5m",
    pg: "PG-17",
    plot:
        "Cecilia's abusive ex-boyfriend fakes his death and becomes invisible to stalk and torment her. She begins experiencing strange events and decides to hunt down the truth on her own.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Spenser",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Iliza Shlesinger",
        "movieName": "Cissy Davis",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Winston Duke",
        "movieName": "Hawk",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 5,
    title: "The Call of the Wild",
    year: 2020,
    poster: "assets/images/poster5.jpg",
    backdrop: "assets/images/backdrop5.jpg",
    genre: "Adventure",
    rating: 6.8,
    genra: ["Family", "Adventure"],
    duration: "1h 40m",
    pg: "PG-13",
    plot:
        "Buck is a big-hearted dog whose blissful domestic life gets turned upside down when he is suddenly uprooted from his California home and transplanted to the exotic wilds of the Alaskan Yukon in the 1890s. ",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 6,
    title: "The Way Back",
    year: 2020,
    poster: "assets/images/poster5.jpg",
    backdrop: "assets/images/backdrop6.jpg",
    genre: "Drama",
    rating: 6.7,
    genra: ["Sport", "Drama"],
    duration: "1h 48m",
    pg: "PG-14",
    plot:
        "Jack Cunningham was a high school basketball superstar who suddenly walked away from the game for unknown reasons.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 7,
    title: "Emma",
    year: 2020,
    poster: "assets/images/poster5.jpg",
    backdrop: "assets/images/backdrop7.jpg",
    genre: "Drama",
    rating: 6.7,
    genra: ["Romance", "Drama"],
    duration: "2h 12m",
    pg: "PG-17",
    plot:
        "Emma. adalah film komedi Inggris-Amerika Serikat tahun 2020 yang disutradarai Autumn de Wilde dan ditulis Eleanor Catton.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 8,
    title: "Bad Boys for Life",
    year: 2020,
    poster: "assets/images/poster5.jpg",
    backdrop: "assets/images/backdrop8.jpg",
    genre: "Action",
    rating: 6.6,
    genra: ["Action", "Comedy"],
    duration: "2h 4m",
    pg: "PG-14",
    plot:
        "Detectives Mike Lowrey and Marcus Burnett join the Miami Police Department's special team AMMO to bring down the ruthless Armando, who is on a mission to kill Mike at his mother Isabel's orders.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 9,
    title: "Sonic the Hedgehog",
    year: 2020,
    poster: "images/poster5.jpg",
    backdrop: "assets/images/backdrop9.jpg",
    genre: "Comedy",
    rating: 6.5,
    genra: ["Family", "Comedy"],
    duration: "1h 39m",
    pg: "PG-14",
    plot:
        "After discovering a small, blue, fast hedgehog, a small-town police officer must help him defeat an evil genius who wants to do experiments on him.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  TopMovie(
    id: 10,
    title: "The Old Guard",
    year: 2020,
    poster: "assets/images/poster5.jpg",
    backdrop: "assets/images/backdrop10.jpg",
    genre: "Horror",
    rating: 6.4,
    genra: ["Action", "Adventure", "Fantasy"],
    duration: "1h 35m",
    pg: "PG-14",
    plot:
        "A covert team of immortal mercenaries is suddenly exposed and must now fight to keep their identity a secret just as an unexpected new member is discovered.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
];
