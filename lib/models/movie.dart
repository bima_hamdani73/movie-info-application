// Our movie model
class Movie {
  final int id, year, numOfRatings, criticsReview, metascoreRating;
  final double rating;
  final List<String> genra;
  final String plot, title, poster, backdrop, pg, duration;
  final List<Map> cast;

  Movie({
    this.poster,
    this.backdrop,
    this.title,
    this.id,
    this.year,
    this.numOfRatings,
    this.criticsReview,
    this.metascoreRating,
    this.rating,
    this.genra,
    this.plot,
    this.cast,
    this.pg,
    this.duration,
  });
}

// our demo data movie data
List<Movie> movies = [
  Movie(
    id: 1,
    title: "Underwater",
    year: 2020,
    poster: "assets/images/poster1.jpg",
    backdrop: "assets/images/backdrop_1.jpg",
    numOfRatings: 150212,
    rating: 5.8,
    criticsReview: 50,
    metascoreRating: 76,
    genra: ["Action", "Horor"],
    pg: "PG-13",
    duration: "1h 35m",
    plot:
        "Kepler 822, a research and drilling facility operated by Tian Industries at the bottom of the Mariana Trench, is struck by a strong earthquake. As part of the facility is destroyed by the quake, mechanical engineer Norah Price and her colleagues, Rodrigo and Paul, make their way to the escape pod bay.",
    cast: [
      {
        "orginalName": "William Eubank",
        "movieName": "Director",
        "image": "assets/images/actor_a5.jpg",
      },
      {
        "orginalName": "Kristen Stewart",
        "movieName": "Norah Price",
        "image": "assets/images/actor_a1.jpg",
      },
      {
        "orginalName": "Jessica Henwick",
        "movieName": "Emily",
        "image": "assets/images/actor_a2.jpg",
      },
      {
        "orginalName": "T.J. Miller",
        "movieName": "Paul",
        "image": "assets/images/actor_a3.jpg",
      },
      {
        "orginalName": "Vincent Cassel",
        "movieName": "W. Lucien",
        "image": "assets/images/actor_a4.jpg",
      },
    ],
  ),
  Movie(
    id: 2,
    title: "Extraction",
    year: 2020,
    poster: "assets/images/poster2.jpg",
    backdrop: "assets/images/backdrop_2.jpg",
    numOfRatings: 150212,
    rating: 6.7,
    criticsReview: 50,
    metascoreRating: 76,
    genra: ["Action", "Thriller"],
    pg: "PG-17",
    duration: "1h 56m",
    plot:
        "A black-market mercenary who has nothing to lose is hired to rescue the kidnapped son of an imprisoned international crime lord. But in the murky underworld of weapons dealers and drug traffickers, an already deadly mission approaches the impossible.",
    cast: [
      {
        "orginalName": "Sam Hargrave",
        "movieName": "Director",
        "image": "assets/images/actor_b4.png",
      },
      {
        "orginalName": "Chris Hemsworth",
        "movieName": "Tyler Rake",
        "image": "assets/images/actor_b1.png",
      },
      {
        "orginalName": "Golshifteh Farahani",
        "movieName": "Nik Khan",
        "image": "assets/images/actor_b2.png",
      },
      {
        "orginalName": "Randeep Hooda",
        "movieName": "Saju Rav",
        "image": "assets/images/actor_b3.png",
      },
      {
        "orginalName": "Neha Mahajan",
        "movieName": "Neysa Rav",
        "image": "assets/images/actor_b5.png",
      },
      {
        "orginalName": "David Harbour",
        "movieName": "Gaspar",
        "image": "assets/images/actor_b6.png",
      },
    ],
  ),
  Movie(
    id: 3,
    title: "Bloodshoot",
    year: 2020,
    poster: "assets/images/poster3.jpg",
    backdrop: "assets/images/backdrop_3.jpg",
    numOfRatings: 150212,
    rating: 7.6,
    criticsReview: 50,
    metascoreRating: 79,
    genra: ["Action", "Sci-fi"],
    duration: "1h 49m",
    pg: "PG-13",
    plot:
        "Ray Garrison, an elite soldier killed in battle, is resurrected and given superhuman abilities. As he sets out to get revenge, he uncovers secrets about his life and the people supposedly helping him.",
    cast: [
      {
        "orginalName": "James Mangold",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Matt Damon",
        "movieName": "Carroll",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Christian Bale",
        "movieName": "Ken Miles",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Caitriona Balfe",
        "movieName": "Mollie",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  Movie(
    id: 4,
    title: "Spenser Confidential",
    year: 2020,
    poster: "assets/images/poster4.jpg",
    backdrop: "assets/images/backdrop_4.jpg",
    numOfRatings: 150212,
    rating: 6.6,
    criticsReview: 50,
    metascoreRating: 79,
    genra: ["Action", "Crime"],
    duration: "1h 51m",
    pg: "PG-17",
    plot:
        "Boston police officer Spenser arrives with his partner, Driscoll, to the home of Captain John Boylan. While questioning Boylan regarding the murder of Gloria Weisnewski, Spenser sees Mrs. Boylan's bloody face and beats Boylan. Spenser is brought up on charges, pleads guilty, and is sentenced to prison.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Spenser",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Iliza Shlesinger",
        "movieName": "Cissy Davis",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Winston Duke",
        "movieName": "Hawk",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
  Movie(
    id: 5,
    title: "Monster Hunter",
    year: 2020,
    poster: "assets/images/poster5.jpg",
    backdrop: "assets/images/backdrop_5.jpg",
    numOfRatings: 150212,
    rating: 7.9,
    criticsReview: 50,
    metascoreRating: 79,
    genra: ["Action", "Adventure"],
    duration: "1h 44m",
    pg: "PG-13",
    plot:
        "Captain Artemis and her group of soldiers are pulled into a world where dangerous monsters and humans co-exist. Grappled by danger, they must find a way to escape with help from a mysterious hunter.",
    cast: [
      {
        "orginalName": "Mark Wahlberg",
        "movieName": "Director",
        "image": "assets/images/actor_c1.png",
      },
      {
        "orginalName": "Milla Jovovich",
        "movieName": "Natalie Artemis",
        "image": "assets/images/actor_c2.png",
      },
      {
        "orginalName": "Tony Jaa",
        "movieName": "The Hunter",
        "image": "assets/images/actor_c3.png",
      },
      {
        "orginalName": "Meagan Good",
        "movieName": "Dash",
        "image": "assets/images/actor_c4.png",
      },
    ],
  ),
];
