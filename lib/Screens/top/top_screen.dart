import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:movie_info_app/models/topMovie.dart';

import '../../constants.dart';

class TopScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Top Movie"),
        backgroundColor: Colors.red,
        toolbarHeight: 60,
      ),
      body: Padding(
        padding: const EdgeInsets.all(8.0),
        child: ListView(
            children: moviesTop.map((topMovie) {
          return TextButton(
            onPressed: () {
              //   Navigator.push(context, MaterialPageRoute(builder: (context) {
              //    return DetailsScreen(
              //      movie: Top_Movie,
              //    );
              //    }
              //    ));
            },
            child: Card(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Expanded(
                    flex: 1,
                    child: Image.asset(
                      topMovie.backdrop,
                      height: 115,
                      width: 100,
                    ),
                  ),
                  Expanded(
                    flex: 2,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            topMovie.title,
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Text(
                            topMovie.genre,
                            style: TextStyle(
                              fontSize: 14.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              SvgPicture.asset(
                                "assets/icons/star_fill.svg",
                                height: 20,
                              ),
                              SizedBox(width: kDefaultPadding / 2),
                              Text(
                                "${topMovie.rating}",
                                style: Theme.of(context).textTheme.bodyText2,
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          );
        }).toList()),
      ),
    );
  }
}
